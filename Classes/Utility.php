<?php


namespace DKM\SiteConfigurationBuilder;


use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Configuration\Loader\Exception\YamlFileLoadingException;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Restriction\BackendWorkspaceRestriction;
use TYPO3\CMS\Core\Database\Query\Restriction\HiddenRestriction;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\Page\PageRepository;

class Utility
{



    /**
     * @param $table
     * @param $fields
     * @param array $where
     * @param array $orderBy
     * @param false $fetchFirst
     * @return mixed[]
     */
    static function fetchAll($table, $fields, $where = [], $orderBy = [], $fetchFirst = false) {
        /** @var Connection $connection */
        $connection = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable($table);
        $statement = $connection->select(
            $fields,
            $table,
            $where,
            [],
            $orderBy
        );
        if($fetchFirst) return $statement->fetch();
        return $statement->fetchAll();
    }

    /**
     * @param $table
     * @param $fields
     * @param array $where
     * @param array $orderBy
     * @return mixed[]
     */
    static function fetchFirst($table, $fields, $where = [], $orderBy = []) {
        return self::fetchAll($table, $fields, $where, $orderBy, true);
    }

    /**
     * Returns a list of pages that have 'is_siteroot' set
     *
     * @return array
     */
    static function getAllSitePages(): array
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('pages');
        $queryBuilder->getRestrictions()->removeByType(HiddenRestriction::class);
        $queryBuilder->getRestrictions()->add(GeneralUtility::makeInstance(BackendWorkspaceRestriction::class, 0, false));
        $statement = $queryBuilder
            ->select('*')
            ->from('pages')
            ->where(
                $queryBuilder->expr()->eq('sys_language_uid', 0),
                $queryBuilder->expr()->orX(
                    $queryBuilder->expr()->andX(
                        $queryBuilder->expr()->eq('pid', 0),
                        $queryBuilder->expr()->neq('doktype', PageRepository::DOKTYPE_SYSFOLDER)
                    ),
                    $queryBuilder->expr()->eq('is_siteroot', 1)
                )
            )
            ->orderBy('pid')
            ->addOrderBy('sorting')
            ->execute();

        $pages = [];
        while ($row = $statement->fetch()) {
            $row['rootline'] = BackendUtility::BEgetRootLine((int)$row['uid']);
            array_pop($row['rootline']);
            $row['rootline'] = array_reverse($row['rootline']);
            $i = 0;
            foreach ($row['rootline'] as &$record) {
                $record['margin'] = $i++ * 20;
            }
            $pages[(int)$row['uid']] = $row;
        }
        return $pages;
    }

    /**
     * @param string $fileName
     * @return string
     */
    static function getFileContents(string $fileName): string
    {
        $streamlinedFileName = GeneralUtility::getFileAbsFileName($fileName);
        if (!$streamlinedFileName) {
            throw new YamlFileLoadingException('YAML File "' . $fileName . '" could not be loaded', 1485784246);
        }
        return file_get_contents($streamlinedFileName);
    }

    /**
     * @param $url
     * @param string $forceSchema
     * @return string|string[]
     */
    static function forceSchemaOnURL($url, $forceSchema = 'https') {
        if($forceSchema) {
            if(strpos($url, 'http') === 0) {
                if(strpos($url, 'http:') === 0 && $forceSchema === 'https') {
                    return str_replace('http:', 'https:', $url);
                } else if(strpos($url, 'https:') === 0 && $forceSchema === 'http') {
                    return str_replace('https:', 'http:', $url);
                }
                return $url;
            } else {
                return $forceSchema . '://' . $url;
            }
        } else {
            return $url;
        }
    }

    /**
     * @param $domains
     * @return false|int|string|null
     */
    static function getIndexKeyOfBaseDomain($domains) {
        $baseDomainKey = null;
        foreach ($domains as $key => $domain) {
            // We assume that the main url of the site, is the one which links are created from
            if($domain['forced']) $baseDomainKey = $key;
            if(!$domain['redirectTo']) $alternativeBaseDomainKey[] = $key;
        }
        // Skip migration for this site - no valid domain record found.
        if(!$baseDomainKey && !isset($alternativeBaseDomainKey)){
            return false;
        } else {
            // No base domain found where forced is set, set alternative base domain
            if(!$baseDomainKey && isset($alternativeBaseDomainKey)) $baseDomainKey = current($alternativeBaseDomainKey);
        }
        return $baseDomainKey;
    }
}