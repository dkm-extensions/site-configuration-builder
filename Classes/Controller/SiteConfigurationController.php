<?php


namespace DKM\SiteConfigurationBuilder\Controller;


class SiteConfigurationController extends \TYPO3\CMS\Backend\Controller\SiteConfigurationController
{
    /**
     * @param $identifier
     */
    public function validateIdentifier($identifier, $rootPageId) {
        return $this->validateAndProcessIdentifier(1, $identifier, $rootPageId);
    }

}