<?php
namespace DKM\SiteConfigurationBuilder\Command;

use DKM\SiteConfigurationBuilder\Controller\SiteConfigurationController;
use DKM\SiteConfigurationBuilder\Utility;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Yaml\Yaml;
use TYPO3\CMS\Backend\Configuration\SiteTcaConfiguration;
use TYPO3\CMS\Core\Configuration\SiteConfiguration;
use TYPO3\CMS\Core\Core\Bootstrap;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\DataHandling\DataHandler;
use TYPO3\CMS\Core\Exception;
use TYPO3\CMS\Core\Site\Entity\Site;
use TYPO3\CMS\Core\Site\SiteFinder;
use TYPO3\CMS\Core\TypoScript\Parser\TypoScriptParser;
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\Exception\MissingArrayPathException;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Charset\CharsetConverter;


class MigrateSiteConfigurationCommand extends Command
{
    protected $dataMaps = [];

    /**
     * @var SiteFinder
     */
    protected $siteFinder;

    /** @var array */
    protected $unConfiguredSites;

    /** @var array */
    protected $builderConfiguration;

    /** @var string */
    protected $baseURLSchema;

    /** @var boolean */
    protected $migrateRedirects;

    /** @var string */
    protected $forceRedirectURLSchema;

    /** @var boolean */
    protected $migrateWebsiteTitle;

    /** @var array */
    protected $redirectsInsertData;

    /** @var array */
    protected $BEUserMigrationCfg = [];

    /** @var boolean */
    protected $alwaysIncludeFirstLevelPagesWhenFindingBEUser;

    /**
     * Default constructor
     */
    public function __construct()
    {
        $this->siteFinder = GeneralUtility::makeInstance(SiteFinder::class);
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Migrates to site configuration and sys_redirect from sys_domain and data sources')
            ->setName('site:migrate')
            ->addArgument(
                'configurationFilePath',
                InputArgument::OPTIONAL,
                'Path to the configuration file',
                ''
            );
        parent::configure();
    }

    /**
     * @param InputInterface $input
     */
    protected function prepareData($input) {
        try {
            if(!$configurationFilePath = $input->getArgument('configurationFilePath')) {
                $noConfigurationFile = true;
            } else {
                if(!$content = Utility::getFileContents($configurationFilePath)) {
                    $noConfigurationFile = true;
                };
            };
        } catch (InvalidArgumentException $e) {
            $noConfigurationFile = true;
        }
        if($noConfigurationFile ?? false ) {
            throw new Exception('The configurationFilePath argument needs to point to a valid configuration file. You can find inspiration on how to make your configuration file from the example file in the Configuration folder.');
        }

        $allSites = $this->siteFinder->getAllSites(false);
        $this->unConfiguredSites = Utility::getAllSitePages();

        //Remove sites already configured
        /** @var Site $site */
        foreach ($allSites as $site) {
            if(isset($this->unConfiguredSites[$site->getRootPageId()])) unset($this->unConfiguredSites[$site->getRootPageId()]);
        }

        $this->builderConfiguration = Yaml::parse($content);
        $this->baseURLSchema = $this->builderConfiguration['baseURLSchema'] ?? 'https';
        $this->migrateRedirects = $this->builderConfiguration['migrateRedirects'] ?? true;
        $this->forceRedirectURLSchema = $this->builderConfiguration['forceRedirectURLSchema'] ?? 'https';
        $this->migrateWebsiteTitle = $this->builderConfiguration['migrateWebsiteTitle'] ?? true;
        $this->alwaysIncludeFirstLevelPagesWhenFindingBEUser = $this->builderConfiguration['alwaysIncludeFirstLevelPagesWhenFindingBEUser'] ?? true;
        $BEUser = $this->builderConfiguration['BEUser'] ?? false;
        $BEUserGroup = $this->builderConfiguration['BEUserGroup'] ?? false;
        if($BEUser) $this->BEUserMigrationCfg['perms_userid'] = $BEUser;
        if($BEUserGroup) $this->BEUserMigrationCfg['perms_groupid'] = $BEUserGroup;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        Bootstrap::initializeBackendAuthentication();
        /** @var DataHandler $dataHandler */
        $dataHandler = GeneralUtility::makeInstance(DataHandler::class);
        $this->prepareData($input);

        $siteConfigurations = [];
        $tableData = [];
        foreach ($this->unConfiguredSites as $pageId => $pageData) {
            $cfg = [];
            // Set root page id
            $cfg['rootPageId'] = $pageId;

            $domains = Utility::fetchAll('sys_domain', ['domainName', 'forced', 'redirectTo', 'redirectHttpStatusCode'], ['pid' => $pageId],['sorting' => 'asc']);
            $baseDomainKey = Utility::getIndexKeyOfBaseDomain($domains);
            if(!is_integer($baseDomainKey)) continue;

            // Set base domain
            $cfg['base'] = $this->baseURLSchema . "://" . $domains[$baseDomainKey]['domainName'];

            if(
                $this->migrateWebsiteTitle ||
                ($this->builderConfiguration['sysTemplateUid'] ?? false) ||
                ($this->builderConfiguration['TypoScriptConstants'] ?? false)
            ) {
                if($sys_template = Utility::fetchFirst('sys_template', ['*'], ['pid' => $pageId, 'root' => 1], ['sorting' => 'asc'])) {
                    //Set root sys_template id
                    if($this->builderConfiguration['sysTemplateUid'] ?? false) {
                        $cfg = ArrayUtility::setValueByPath($cfg, $this->builderConfiguration['sysTemplateUid'], $sys_template['uid'], '.');
                    }

                    // Migrate configured constants
                    $this->migrateConstants($sys_template, $cfg);

                    // Migrate website title from sys_template to Site Configuration - https://docs.typo3.org/c/typo3/cms-core/master/en-us/Changelog/10.2/Feature-85592-AddSiteTitleConfigurationToSitesModule.html
                    if($this->migrateWebsiteTitle) {
                        $cfg['websiteTitle'] = $sys_template['sitetitle'];
                    }
                }
            }

            // Add main be_user and be_group to cfg
            $this->addSiteBEUser($pageId, $cfg);

            //TODO add imports
            if(($this->builderConfiguration['imports'] ?? false) && is_array($this->builderConfiguration['imports'])) {
                $cfg['imports'] = $this->builderConfiguration['imports'];
            }

            //TODO baseVariants
            $this->addBaseVariants($cfg);

            // Add configuration for site to array
            $siteConfigurations[$pageId] = $cfg;

            $this->addRedirectsData($domains, $baseDomainKey);
        }

        //Create site configuration files from $siteConfigurations array
        $this->createSiteConfigurationFiles($siteConfigurations);
        foreach ($siteConfigurations as $siteConfiguration) {
            $tableData[] = array_intersect_key($siteConfiguration, ['websiteTitle'=>1, 'base'=>1]);
        }
	if($tableData) {
            $output->writeln('Site Configuration created for:');
            $table = new Table($output);
            $table
                ->setHeaders(['base', 'webSiteTitle'])
                ->setRows($tableData)
                ->render();

            // If redirects data, create the redirects
            if($this->redirectsInsertData) {
                $dataHandler->start($this->redirectsInsertData ?? [], []);
                $dataHandler->process_datamap();
                $output->writeln(count($this->redirectsInsertData['sys_redirect']) . ' redirect records created.');
            }
        } else {
            $output->writeln('Nothing to migrate. Maybe you already did it?');
        }
    }

    /**
     * TODO make code...
     * @param array $siteConfigurationArray
     */
    protected function createSiteConfigurationFiles($siteConfigurationArray) {
        $GLOBALS['TCA'] = array_merge($GLOBALS['TCA'], GeneralUtility::makeInstance(SiteTcaConfiguration::class)->getTca());
        /** @var SiteConfigurationController $siteConfigurationController */
        $siteConfigurationController = GeneralUtility::makeInstance(SiteConfigurationController::class);

        $csConverter = GeneralUtility::makeInstance(CharsetConverter::class);
        foreach ($siteConfigurationArray as $pageId => $arr) {
            $siteIdentifier = $arr['websiteTitle'] ?: parse_url($arr['base'], PHP_URL_HOST);
            $siteIdentifier = $csConverter->specCharsToASCII('utf-8', $siteIdentifier);
            $siteIdentifier = $siteConfigurationController->validateIdentifier($siteIdentifier, $pageId);
            // Persist the configuration
            $siteConfigurationManager = GeneralUtility::makeInstance(SiteConfiguration::class, Environment::getConfigPath() . '/sites');
            $siteConfigurationManager->write($siteIdentifier, $arr);
        }

    }

    /**
     * @param int $rootPageUid
     * @param array $cfg
     */
    protected function addSiteBEUser($rootPageUid, &$cfg) {
        foreach($this->BEUserMigrationCfg as $field => $siteConfigurationPath) {
            /** @var QueryBuilder $queryBuilder */
            $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('pages');
            $queryBuilder->getRestrictions()->removeAll();

            $leftJoinOn[] = $queryBuilder->expr()->eq('p1.uid', $queryBuilder->quoteIdentifier('p2.pid'));
            if(!$this->alwaysIncludeFirstLevelPagesWhenFindingBEUser) {
                $leftJoinOn[] = $queryBuilder->expr()->eq('p1.' . $field, $queryBuilder->createNamedParameter(0));
            }
            $queryBuilder->from('pages', 'p1')
                ->add('select', "IF(p2.{$field}, p2.{$field}, p1.{$field}) as {$field}2")
                ->leftJoin('p1', 'pages', 'p2', $queryBuilder->expr()->andX(...$leftJoinOn))
                ->where($queryBuilder->expr()->eq('p1.uid', $queryBuilder->createNamedParameter($rootPageUid)))
                ->addGroupBy("{$field}2")
                ->add('orderBy', "count(p1.{$field}) desc")
                ->setMaxResults(1);
            if($uid = $queryBuilder->execute()->fetchColumn()) {
                $cfg = ArrayUtility::setValueByPath($cfg, $siteConfigurationPath, $uid, '.');
            }
        }
    }

    /**
     * @param $cfg
     * @throws Exception
     */
    protected function addBaseVariants(&$cfg) {
        $baseUrlNoTopLevel = str_replace('//www.','//', $cfg['base']);
        $baseUrlNoTopLevel = substr($baseUrlNoTopLevel, 0, strpos($baseUrlNoTopLevel, "."));
 
        if(($this->builderConfiguration['baseVariants'] ?? false) && is_array($this->builderConfiguration['baseVariants'])) {
            foreach ($this->builderConfiguration['baseVariants'] as $baseVariantCfg) {
                if(!(isset($baseVariantCfg['baseReplaceTopLevelWith']) && $baseVariantCfg['condition'])) {
                    throw new Exception('Configuration for baseVariant is not complete!');
                };
                $baseUrl = "{$baseUrlNoTopLevel}.{$baseVariantCfg['baseReplaceTopLevelWith']}";
                $cfg['baseVariants'][] = [
                    'base' => Utility::forceSchemaOnURL(
                        $baseUrl,
                        $baseVariantCfg['baseURLSchema'] ?? false
                    ),
                    'condition' => $baseVariantCfg['condition']
                ];
            }
        }
    }

    /**
     * @param $domains
     * @param $baseDomainKey
     */
    protected function addRedirectsData($domains, $baseDomainKey) {
        static $createId = 'NEW9823be80';
        // Create data array for inserting new redirect records
        if($this->migrateRedirects && ExtensionManagementUtility::isLoaded('redirects')) {
            foreach ($domains as $key => $domain) {
                // The base domain should not be created as redirect
                if($key == $baseDomainKey) continue;
                // non-www domain should not be created as redirect (use .htaccess redirect instead)
                if(substr_count($domain['domainName'], '.') <= 1) continue;

                $target = rtrim(Utility::forceSchemaOnURL($domain['redirectTo'], $this->forceRedirectURLSchema), '/');

                $parsedTarget = parse_url($target);
                // Domain to domain redirect, then add source url path to target
                if(!isset($parsedTarget['path'])) {
                    $target .= '/$1';
                }
                $this->redirectsInsertData['sys_redirect'][$createId++] = [
                    'pid' => 0,
                    'source_host' => $domain['domainName'],
                    'source_path' => '#(.*)#',
                    'is_regexp' => 1,
                    'force_https' => 0,
                    'target' => $target,
                    'target_statuscode' => $domain['redirectHttpStatusCode']
                ];
            }
        }
    }

    /**
     * @param array $sys_template
     * @param array $cfg
     */
    protected function migrateConstants($sys_template, &$cfg) {
        if(!$sys_template) return;
        if(($this->builderConfiguration['TypoScriptConstants'] ?? false) and is_array($this->builderConfiguration['TypoScriptConstants'])) {
            // Parse TS constants string to $constants array
            /** @var TypoScriptParser $constants */
            $constants = GeneralUtility::makeInstance(TypoScriptParser::class);
            $constants->parse($sys_template['constants']);
            $constants = GeneralUtility::removeDotsFromTS($constants->setup);

            // Find and migrate all Constatns configurations
            foreach ($this->builderConfiguration['TypoScriptConstants'] as $constantsPath => $siteConfigurationPath) {
                $value = null;
                try {
                    $value = ArrayUtility::getValueByPath($constants, $constantsPath, '.');
                } catch (MissingArrayPathException $e) {
                }
                if($value) {
                    $cfg = ArrayUtility::setValueByPath($cfg, $siteConfigurationPath, $value, '.');
                }
            }
        }
    }
}
