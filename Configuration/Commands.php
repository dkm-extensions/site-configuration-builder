<?php
return [
    'site:migrate' => [
        'class' => DKM\SiteConfigurationBuilder\Command\MigrateSiteConfigurationCommand::class,
        'schedulable' => true,
    ],
];